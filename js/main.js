$(function() {
    console.log( "ready!" );
});

function scrollToExperience() {
    $('html,body').animate({
            scrollTop: $(".career").offset().top},
        'slow');
}

function scrollToEducation() {
    $('html,body').animate({
            scrollTop: $(".education").offset().top},
        'slow');
}

function scrollToCertification() {
    $('html,body').animate({
            scrollTop: $(".certificate").offset().top},
        'slow');
}